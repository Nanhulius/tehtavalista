<?php require_once 'inc/top.php'; ?>
<h3>Poista tehtäviä</h3>
<?php
    try {
        $idt = filter_input(INPUT_POST, 'id', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $i = 0;
        if ($idt) {
            foreach ($idt as $id) {
                if (is_numeric($id)) {
                    if (strlen($i) > 0) {
                        $i .= " OR ";
                    }
                    $i .= "id = $id";
                }
            }
        }
        if (strlen($i) > 0) {
            $sql = "DELETE FROM task WHERE $i;";
            $query = $db -> query($sql);
            $query -> execute();
        }
        print "<p>Valitut tehtävät poistettu listalta!</p>";
    } catch (PDOException $pdoex) {
        print "<p>Tietojen päivittäminen epäonnistui. " . $pdoex -> getMessage() . "</p>";
    }
?>
<a href="index.php">Tehtävälistaan</a>
<?php require_once 'inc/bottom.php'; ?>